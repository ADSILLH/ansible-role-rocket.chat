Deploy Rocket.Chat with Ansible and Docker!  

## Features
**Full stack deployment:**  
Fully deploy Rocket.Chat, including MongoDB & an Nginx reverse SSL proxy, yes it's so cool! 

## Backup & Restore

Backup:  
`docker exec debian_mongo_1 bash -c 'mongodump  --db rocketchat --out /data/db/dump'`

Restore:  
`docker exec debian_mongo_1 bash -c 'mongorestore  --db rocketchat --dir /data/db/dump/rocketchat'`

## Automatically update Docker images

Add cron job rules:  
`@weekly /usr/local/bin/docker-compose pull && /usr/local/bin/docker-compose up -d --remove-orphans && /usr/bin/docker image prune -f`


## TODO
* ~~add auto-update~~
* create auto backup
* configure the chat bot

## License
MIT License. See the [LICENSE file](LICENSE) for details.  

Enjoy!
